# Helper Commands

This looks for speeding and summarizing commond task when working with git

## Installing

make sure that the helper-commands.sh file gets executed, one good place to call it is in your bash profile

## Release notes

Please go [here](CHANGELOG.md) to see the project full release notes

## Authors

* **Luxant** - *Author*

## License

Extend it, improve it